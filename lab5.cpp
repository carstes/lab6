#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   Card deck1[52];

   //Initializes each of the cards to a value
   for(int i = 0; i<4;i++){
     for(int x = 1; x<14; x++){
        switch (i) {
          case 0:  deck1[i*14 + x-1].suit = SPADES; break;
          case 1:  deck1[i*14 + x-1].suit = HEARTS; break;
          case 2:  deck1[i*14 + x-1].suit = DIAMONDS; break;
          case 3:  deck1[i*14 + x-1].suit = CLUBS; break;
        }
        if(x == 1){ deck1[i*14 + x-1].value = 14;
        } else { deck1[i*14 + x-1].value = x; }
    }  }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   
   random_shuffle(&deck1[0],&deck1[52],myrandom);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    Card hand[5] = {deck1[0],deck1[1],deck1[2],deck1[3],deck1[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
	sort(&hand[0],&hand[5],suit_order);
	/*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     for(int i = 0; i < 5; i++){
       cout << get_card_name(hand[i])<< setw(10)<<endl;
     }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  return lhs.suit< rhs.suit;
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  string name = "";

  switch(c.value){
    case 2: name = "2"; break;
    case 3: name = "3"; break;
    case 4: name = "4"; break;
    case 5: name = "5"; break;
    case 6: name = "6"; break;
    case 7: name = "7"; break;
    case 8: name = "8"; break;
    case 9: name = "9"; break;
    case 10: name = "10"; break;
    case 11: name = "Jack"; break;
    case 12: name = "Queen"; break;
    case 13: name = "King"; break;
    case 14: name = "Ace"; break;
    default: name = "Error ";

  }

  return(name + " of " + get_suit_code(c));
}
